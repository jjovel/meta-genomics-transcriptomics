# APPENDIX 7

# This workflow runs kraken2 on fastq files and parses the results
# For installation see: http://ccb.jhu.edu/software/kraken/MANUAL.html#installation

########## STEP 1. Create a kraken2 database

# The simplest way to crate a kraken2 database is building a standard database, through the commands:

DBNAME='kraken2_standard_yymmdd' # e.g. kraken2_standard_180910

# This commands will take few hours to build the database in the current directory
# a connection to internet is required

kraken2-build --standard --db $DBNAME 

########## STEP 2. Run kraken2
# Sequences should be quality-trimmed. The quality threshold and software and optional.
# Here is the quality trimming command we used (file 'adapters.fa' is provided in Appendix 5):
fastq-mcf adapters.fa -o sample_R1_trimmed.fastq.gz -o sample_R2_trimmed.fastq.gz sample_R1.fastq.gz sample_R2.fastq.gz -k 0 -l 120 -w 3 -q 24

### 2.1 Load Kraken2 database onto memory
# If many libraries are being run, it makes sense to load the kraken2 database onto memory.
# To do so, create a copy of the database in memory (only files opts.k2d, hash.k2d and taxo.k2d are required)

cp ${DBNAME}/*k2d /dev/shm/

# Assuming libraries names with the following structure 'sampleName_R1.fq.gz, sampleName_R2.fq.gz'
# the following command line can be used to run all libraries at once

# If metaphlan-like ouput is preferred
for FILE in $(ls *_R1.fq.gz | sed 's/_R1.fq.gz//'); do kraken2 --db $DBNAME --memory-mapping --threads 16 --use-mpa-style --confidence 0.1 --report ${FILE}_kraken2.tax --paired ${FILE}_R1.fastq.gz ${FILE}_R2.fastq.gz --output ${FILE}_kraken2.txt; done

# If classic Kraken output is preferred
for FILE in $(ls *_R1.fq.gz | sed 's/_R1.fq.gz//'); do kraken2 --db $DBNAME --memory-mapping --threads 16 --use-names --confidence 0.1 --report ${FILE}_kraken2.tax --paired ${FILE}_R1.fastq.gz ${FILE}_R2.fastq.gz --output ${FILE}_kraken2.txt; done


### IMPORTANT: remember to remove '*kd2' files from /dev/shm/ after running Kraken2, otherwise they will be using memory permanently

# The previous command will produce two series of result files: one with suffix '_kraken2.txt', which contain the standard Kraken results
# and another series with the suffix '_kraken2.tax', which will contain taxa abundances in a metaphlan-like format or in classic kraken format


# To consolidate all samples results in a single table, the script 'merge_metaphlan_tables.py' from metaphlan2 pipeline can be used.
# For convenience, we included such script in Appendix5. Save it in a file called merge_metaphlan_tables.py, if you want to use it, with the
# following command line:

python merge_tables.py *parsed.txt > consolidated_table.tsv

# The table 'consolidated_table.tsv', after normalization, may be used for principal coordinate analysis
# (using Bray-Curtis distances) and PERMANOVA analysis between groups, using the R script included in Appendix7.   


