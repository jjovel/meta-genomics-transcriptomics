# Metagenomics versus metatranscriptomics of the murine gut microbiome for assessing microbial pathways metabolism during inflammation #

This repository contains all the information required to reproduce all the analyses included in our publication in Frontiers in Microbioly. If you need additinal information, please contact the corresponding authors.

### Descrition of scripts and files ###

* adapters.fa: A FastA file containing all Illumina adapters needed fro trimming low quality bases.
* merge_tables.py: A script (from the Hutterhower lab) to merge count tables.
* run_humann2.sh: All command lines needed to run HUMAnN2.
* run_kraken.sh: All command lines needed to run kraken toxonomy classifier.
* statistical_analysis_and_PCoA.R: It performs normalization of count table, PCoA, PERMANOVA analyses.

### Questions to: ###
jovel@ualberta.ca

