# APPENDIX 10

# This workflow show how to run HUMAnN2 of quality-trimmed fastq files
# For installation, visit the HUMAnN bitbucket page: https://bitbucket.org/biobakery/humann2/wiki/Home

# We quality-trimmed our files with the following command 
# (requires file 'adapters.fa' included in Appendix5 and  installation of ea-utils: https://expressionanalysis.github.io/ea-utils/):
fastq-mcf adapters.fa -o sample_R1_trimmed.fastq.gz -o sample_R2_trimmed.fastq.gz sample_R1.fastq.gz sample_R2.fastq.gz -k 0 -l 120 -w 3 -q 24

# HUMAnN2 does not uses the paired-end information of paired-end reads, so it is best to concatenate
# end1 and end2 of each library in a single file for running HUMAnN2 (using command 'cat': e.g. cat <sample_R1.fq> <sample_R2.fq> > sample_12.fq

# Once ends are concatenated, the HUMAnN2 pipeline can be run as follows:

########## STEP 1. Run humann2

humann2 --input <fastq-file> --output <outFile> --threads 12

# If FILE names contain the following structure 'sample.fq', all libraries can be run with this command:

for FILE in *fq; do humann2 --input $FILE --output ${FILE/.fq/_humann2} --threads 12; done

# For each sample, three output files will be generated: 
# 'sample_genefamilies.tsv'
# 'sample_pathabundance.tsv'
# 'sample_pathcoverage.tsv'


########## STEP 3. Consolidate all results in a single table

# Put all genefamilies files in a single directory:

mkdir geneFamilies

for DIR in *_humann2; do cp ${DIR}/${DIR}_genefamilies.tsv ./geneFamilies; done

# To consolidate files for all samples in a single table (for example for gene families):

python merge_tables.py  *_genefamilies.tsv > ../humann2_genefam_cons.tsv

cd ..
rm -r geneFamilies

# Results are in table 'humann2_genefam_cons.tsv'

